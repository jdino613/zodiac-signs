<?php  
	$fullName = $_POST['fullName'];
	$birthMonth = $_POST['birthMonth'];
	$birthDay = $_POST['birthDay'];

	$zodiac = "";
	   if ( ( $birthMonth == "March" && $birthDay > 20 ) || ( $birthMonth == "April" && $birthDay < 20 ) ) { $zodiac = "Aries"; } 
   elseif ( ( $birthMonth == "April" && $birthDay > 19 ) || ( $birthMonth == "May" && $birthDay < 21 ) ) { $zodiac = "Taurus"; } 
   elseif ( ( $birthMonth == "May" && $birthDay > 20 ) || ( $birthMonth == "June" && $birthDay < 21 ) ) { $zodiac = "Gemini"; } 
   elseif ( ( $birthMonth == "June" && $birthDay > 20 ) || ( $birthMonth == "July" && $birthDay < 23 ) ) { $zodiac = "Cancer"; } 
   elseif ( ( $birthMonth== "July" && $birthDay > 22 ) || ( $birthMonth == "August" && $birthDay < 23 ) ) { $zodiac = "Leo"; } 
   elseif ( ( $birthMonth == "August" && $birthDay > 22 ) || ( $birthMonth == "September" && $birthDay < 23 ) ) { $zodiac = "Virgo"; } 
   elseif ( ( $birthMonth == "September" && $birthDay > 22 ) || ( $birthMonth == "October" && $birthDay < 23 ) ) { $zodiac = "Libra"; } 
   elseif ( ( $birthMonth == "October" && $birthDay > 22 ) || ( $birthMonth == "November" && $birthDay < 22 ) ) { $zodiac = "Scorpio"; } 
   elseif ( ( $birthMonth == "November" && $birthDay > 21 ) || ( $birthMonth == "December" && $birthDay < 22 ) ) { $zodiac = "Sagittarius"; } 
   elseif ( ($birthMonth == "December" && $birthDay > 21 ) || ( $birthMonth== "January" && $birthDay < 20 ) ) { $zodiac = "Capricorn"; } 
   elseif ( ( $birthMonth == "January" && $birthDay > 19 ) || ( $birthMonth == "February" && $birthDay < 19 ) ) { $zodiac = "Aquarius"; } 
   elseif ( ( $birthMonth == "February" && $birthDay > 18 ) || ( $birthMonth == "March" && $birthDay < 21 ) ) { $zodiac = "Pisces"; } 
	

	session_start();
	if(strlen($fullName)===0 || strlen($birthMonth)===0 || strlen($birthDay)===0){
		header("Location: ". $_SERVER['HTTP_REFERER']);
		$_SESSION['errorMsg']="Please fill up the form properly";
	}else{
		
	$_SESSION['fullName'] = $fullName;
	$_SESSION['birthMonth'] = $birthMonth;
	$_SESSION['birthDay'] = $birthDay;
	$_SESSION['zodiac'] = $zodiac;


	header("Location: ../views/landingpage.php");

}
?>
