<!DOCTYPE html>
<html>
<head>
	<title>Zodiac Sign</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/bootstrap.css">
</head>
<body>


<?php
$pic = "images/zodiac.jpg";
?>

<html>
<head>
<style type="text/css">

body {
background-image: linear-gradient(rgba(40, 40, 54,0.5), rgba(40, 40, 54,0.5)), url('<?php echo $pic;?>');
background-position: center;
background-size: cover;

}

</style>

	<h1 class="text-center text-warning my-4">What's Your Zodiac Sign?</h1>
	<div class="col-lg-4 offset-lg-4">

	<form action="controllers/process.php" method="POST">
		<div class="form-group">
				<label for="fullName" class="text-white">Name</label>
				<input type="text" name="fullName" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth" class="text-white">Birth Month</label>
				<input type="text" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthDay" class="text-white">Birth Day</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-danger">Check Zodiac</button>
			</div>

			<?php 
				session_start();
				session_destroy();
				if(isset($_SESSION['errorMsg'])){
			?>
				

			<p class="text-danger"> <?php echo $_SESSION['errorMsg']?></p>
			
			<?php
				
				}

			?>
	</form>
		</div>
</body>
</html>